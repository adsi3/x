package com.adsi.bike.service;


import com.adsi.bike.domain.Rols;
import com.adsi.bike.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IRolsServiceImp implements IRolsService {

    @Autowired
    RolRepository rolRepository;

    @Override
    public Iterable<Rols> read() {
        return rolRepository.findAll();
    }

    @Override
    public Optional<Rols> getById(Integer id) {
        return rolRepository.findById(id);
    }

    @Override
    public ResponseEntity create(Rols rols) {
        return new ResponseEntity(rolRepository.save(rols), HttpStatus.OK);
    }

    @Override
    public Rols update(Rols rols) {
        return rolRepository.save(rols);
    }


}
