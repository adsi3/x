package com.adsi.bike.service;

import com.adsi.bike.domain.Bike;
import com.adsi.bike.repository.BikeRepository;
import com.adsi.bike.service.dto.BikeDTO;
import com.adsi.bike.service.transformer.BikeTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
public class IBikeServiceImp implements IBikeService {

    //all business logic

    @Autowired
    BikeRepository bikeRepository;



    @Override
    public Page<BikeDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return bikeRepository.findAll(pageable)
                .map(BikeTransformer::getBikeDTOFromBike);
    }

    @Override
    public Bike update(Bike bike) {
        return bikeRepository.save(bike);
    }

    @Override
    public void delete(Integer id) {
        bikeRepository.deleteById(id);
    }

    @Override
    public ResponseEntity create(BikeDTO bikeDTO) {
        Bike bike = BikeTransformer.getBikeFromDTO(bikeDTO);
        if (bikeRepository.findBySerial(bike.getSerial()).isPresent()){
            return new ResponseEntity("El serial ya existe", HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity(BikeTransformer.getBikeDTOFromBike(bikeRepository.save(bike)),HttpStatus.OK);
        }

    }

    @Override
    public Optional<Bike> getById(@PathVariable Integer id) {
        return bikeRepository.findById(id);
    }

    @Override
    public ResponseEntity search(String serial, String model) {
        if (serial != null && model != null){
            return new ResponseEntity(bikeRepository.findBySerialContainsAndModelContains(serial, model), HttpStatus.OK);
        }else if (serial != null){
            return new ResponseEntity(bikeRepository.findBySerialContains(serial), HttpStatus.OK);
        }else if (model != null){
            return new ResponseEntity(bikeRepository.findByModelContains(model),HttpStatus.OK);
        }else{
            return new ResponseEntity("No ha datos suficientes para la consulta", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public Integer quantityBikes() {
        return bikeRepository.quantityBikes();
    }

    @Override
    public Double inventoryValue() {
        return bikeRepository.inventoryValue();
    }

    @Override
    public Iterable<Bike> findByModelQuery(String model) {
        return bikeRepository.findByModelQuery(model);
    }
}
