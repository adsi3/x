package com.adsi.bike.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BikeDTO {

    @NotNull
    private int id;

    private String model;
    private String serial;
    private Double price;


}
