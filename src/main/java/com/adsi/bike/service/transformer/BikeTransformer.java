package com.adsi.bike.service.transformer;

import com.adsi.bike.domain.Bike;
import com.adsi.bike.service.dto.BikeDTO;

public class BikeTransformer {

    public static BikeDTO getBikeDTOFromBike(Bike bike){
        if(bike == null){
            return null;
        }
        BikeDTO dto = new BikeDTO();

        //set variables
        dto.setId(bike.getId());
        dto.setModel(bike.getModel());
        dto.setPrice(bike.getPrice());
        dto.setSerial(bike.getSerial());

        return dto;
    }

    public static Bike getBikeFromDTO(BikeDTO bikeDTO){
        if(bikeDTO == null){
            return null;
        }
        //create Object of Bike
        Bike bike = new Bike();

        bike.setId(bikeDTO.getId());
        bike.setModel(bikeDTO.getModel());
        bike.setPrice(bikeDTO.getPrice());
        bike.setSerial(bikeDTO.getSerial());

        return bike;
    }
}
